<?php

declare(strict_types=1);

$app = require __DIR__ . '/../private/app/bootstrap.php';
$app->get('cli_frontend')->run();
