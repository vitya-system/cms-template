<?php

/*
 * Copyright 2021, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

require __DIR__ . '/functions.inc.php';

$app = require __DIR__ . '/../../private/app/bootstrap.php';

echo "\r\n";
echo '────────────────────────────────────────────────────────────────────────────────' . "\r\n";
echo "\r\n";

passthru('php ' . realpath(__DIR__ . '/..') . '/vitya.php install');

echo "\r\n";
echo '────────────────────────────────────────────────────────────────────────────────' . "\r\n";
echo "\r\n";

try {
    $admin_user = $app->get('entity')->make('App\User\User');
    $admin_user->getComponent('first_name')->set('Administrator');
    $admin_user->getComponent('identity')->set(['username' => 'admin']);
    $admin_user->getComponent('identity')->setClearPassword('123456789');
    $admin_user->getComponent('user_management')->setOmnipotent(true);
    $admin_user->setFullyCreated(true);
    $admin_user = $admin_user->create();
} catch (Exception $e) {
    echo 'It seems that the project is already initialized.';
    echo "\r\n";
    exit();
}

$answer = 'x';
while (false === in_array($answer, ['y', 'Y', 'n', 'N', ''])) {
    $answer = trim(readline('Do you wish to add somme sample content? (Y/n) '));
}
if ('n' === strtolower($answer)) {
    exit();
}

$image_1 = $app->get('entity')->make('App\Image\Image');
$image_1->getComponent('legend')->getChild('en')->set('Butterfly');
$image_1->getComponent('legend')->getChild('fr')->set('Papillon');
$image_1->setFullyCreated(true);
$image_1 = $image_1->create();
$image_1->getComponent('image')->setSourceFile(__DIR__ . '/butterfly.jpg');
$image_1->save();

$image_2 = $app->get('entity')->make('App\Image\Image');
$image_2->getComponent('legend')->getChild('en')->set('Ladybug');
$image_2->getComponent('legend')->getChild('fr')->set('Coccinelle');
$image_2->setFullyCreated(true);
$image_2 = $image_2->create();
$image_2->getComponent('image')->setSourceFile(__DIR__ . '/ladybug.jpg');
$image_2->save();

$sample_publication = [
    'publishable' => true,
    'start_uts' => time(),
    'end_uts' => null,
];

$article_1 = $app->get('entity')->make('App\Article\Article');
$article_1->getComponent('title')->getChild('en')->set('Lorem ipsum for beginners');
$article_1->getComponent('title')->getChild('fr')->set('Le lorem ipsum pour les débutants');
$article_1->getComponent('author')->set('Alice Doe');
$article_1->getComponent('teaser')->getChild('en')->set(generate_html_lorem_ipsum(1));
$article_1->getComponent('teaser')->getChild('fr')->set(generate_html_lorem_ipsum(1));
$article_1->getComponent('body')->set(generate_composite_list_field_content());
$article_1->getComponent('body')->set(generate_composite_list_field_content());
$article_1->getComponent('main_image')->set([
    [
        'class' => get_class($image_1),
        'id' => $image_1->getId(),
    ]
]);
$article_1->getComponent('publication')->getChild('en')->set($sample_publication);
$article_1->getComponent('publication')->getChild('fr')->set($sample_publication);
$article_1->setFullyCreated(true);
$article_1->create();

$article_2 = $app->get('entity')->make('App\Article\Article');
$article_2->getComponent('title')->getChild('en')->set('A brief history of lorem ipsum');
$article_2->getComponent('title')->getChild('fr')->set('L\'histoire du lorem ipsum');
$article_2->getComponent('author')->set('Bob Doe');
$article_2->getComponent('teaser')->getChild('en')->set(generate_html_lorem_ipsum(1));
$article_2->getComponent('teaser')->getChild('fr')->set(generate_html_lorem_ipsum(1));
$article_2->getComponent('body')->set(generate_composite_list_field_content());
$article_2->getComponent('body')->set(generate_composite_list_field_content());
$article_2->getComponent('main_image')->set([
    [
        'class' => get_class($image_2),
        'id' => $image_2->getId(),
    ]
]);
$article_2->getComponent('publication')->getChild('en')->set($sample_publication);
$article_2->getComponent('publication')->getChild('fr')->set($sample_publication);
$article_2->setFullyCreated(true);
$article_2->create();

$static_page_1 = $app->get('entity')->make('App\StaticPage\StaticPage');
$static_page_1->getComponent('title')->getChild('en')->set('About this website');
$static_page_1->getComponent('title')->getChild('fr')->set('À propos de ce site');
$static_page_1->getComponent('teaser')->getChild('en')->set(generate_html_lorem_ipsum(1));
$static_page_1->getComponent('teaser')->getChild('fr')->set(generate_html_lorem_ipsum(1));
$static_page_1->getComponent('body')->getChild('en')->set(generate_html_lorem_ipsum(4));
$static_page_1->getComponent('body')->getChild('fr')->set(generate_html_lorem_ipsum(4));
$static_page_1->getComponent('publication')->getChild('en')->set($sample_publication);
$static_page_1->getComponent('publication')->getChild('fr')->set($sample_publication);
$static_page_1->setFullyCreated(true);
$static_page_1->create();

$static_page_2 = $app->get('entity')->make('App\StaticPage\StaticPage');
$static_page_2->getComponent('title')->getChild('en')->set('Contact us');
$static_page_2->getComponent('title')->getChild('fr')->set('Nous contacter');
$static_page_2->getComponent('teaser')->getChild('en')->set(generate_html_lorem_ipsum(1));
$static_page_2->getComponent('teaser')->getChild('fr')->set(generate_html_lorem_ipsum(1));
$static_page_2->getComponent('body')->getChild('en')->set(generate_html_lorem_ipsum(1));
$static_page_2->getComponent('body')->getChild('fr')->set(generate_html_lorem_ipsum(1));
$static_page_2->getComponent('publication')->getChild('en')->set($sample_publication);
$static_page_2->getComponent('publication')->getChild('fr')->set($sample_publication);
$static_page_2->setFullyCreated(true);
$static_page_2->create();

// Navigation menu.
$navigation_menu_item_1 = $app->get('entity')->make('App\NavigationMenu\NavigationMenuItem');
$navigation_menu_item_1->getComponent('title')->getChild('en')->set('About');
$navigation_menu_item_1->getComponent('title')->getChild('fr')->set('À propos');
$navigation_menu_item_1->getComponent('url')->getChild('en')->set($app->get('router')->createUri('static-page-view', ['locale' => 'en', 'id' => 1], []));
$navigation_menu_item_1->getComponent('url')->getChild('fr')->set($app->get('router')->createUri('static-page-view', ['locale' => 'fr', 'id' => 1], []));
$navigation_menu_item_1->setFullyCreated(true);
$navigation_menu_item_1->create();

$navigation_menu_item_2 = $app->get('entity')->make('App\NavigationMenu\NavigationMenuItem');
$navigation_menu_item_2->getComponent('title')->getChild('en')->set('Contact');
$navigation_menu_item_2->getComponent('title')->getChild('fr')->set('Contact');
$navigation_menu_item_2->getComponent('url')->getChild('en')->set($app->get('router')->createUri('static-page-view', ['locale' => 'en', 'id' => 2], []));
$navigation_menu_item_2->getComponent('url')->getChild('fr')->set($app->get('router')->createUri('static-page-view', ['locale' => 'fr', 'id' => 2], []));
$navigation_menu_item_2->setFullyCreated(true);
$navigation_menu_item_2->create();

// Singletons are created during the install process.
$configuration = $app->get('entity')->load('App\Configuration\Configuration', 1);
$configuration->getComponent('website_title')->getChild('en')->set('My website');
$configuration->getComponent('website_title')->getChild('fr')->set('Mon site web');
$configuration->getComponent('website_description')->getChild('en')->set('This is my website, powered by Vitya.');
$configuration->getComponent('website_description')->getChild('fr')->set('Voici mon site web, créé avec Vitya.');
$configuration->setFullyCreated(true);
$configuration->save();

echo 'Sample content created.' . "\r\n";

echo "\r\n";
echo '────────────────────────────────────────────────────────────────────────────────' . "\r\n";
echo "\r\n";

echo "\033[45m\033[37;1m" . '                                                                                ' . "\033[0m" . "\r\n";
echo "\033[45m\033[37;1m" . '  Your Vitya application is ready!                                              ' . "\033[0m" . "\r\n";
echo "\033[45m\033[37;1m" . '                                                                                ' . "\033[0m" . "\r\n";
echo "\r\n";
echo '  You can start using it right now with PHP built-in web server. Type the ' . "\r\n";
echo '  following command in your terminal:' . "\r\n";
echo "\r\n";
echo '  ' . 'php ' . realpath(__DIR__ . '/..') . '/server.php' . "\r\n";
echo "\r\n";
