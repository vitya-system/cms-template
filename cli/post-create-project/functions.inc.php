<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

 declare(strict_types=1);

 function generate_html_lorem_ipsum(int $nb_paragraphs): string
 {
    $html = '';
    $source = file_get_contents(__DIR__ . '/lorem.txt');
    $txt_blocks = explode("\n", $source);
    shuffle($txt_blocks);
    $k = count($txt_blocks);
    $txt_block_counter = 0;
    for ($i = 0; $i < $nb_paragraphs; $i++) {
        $txt_block_counter++;
        while ('' === $txt_blocks[$txt_block_counter % $k]) {
            $txt_block_counter++;
        }
        $html .= '<p>' . htmlspecialchars($txt_blocks[$txt_block_counter % $k]) . '</p>' . "\n";
    }
    return $html;
 }

 function generate_composite_id(): string
 {
    return 'c' . md5(uniqid());
 }

 function generate_composite_list_field_content(): array
 {
    $nb_elements = rand(2, 5);
    $a = [];
    for ($i = 0; $i < $nb_elements; $i++) {
        $id = generate_composite_id();
        $a[$id] = [
            'class' => 'App\Composite\TextComposite\TextComposite',
            'content' => [
                'html' => [
                    'en' => generate_html_lorem_ipsum(rand(1, 3)),
                    'fr' => generate_html_lorem_ipsum(rand(1, 3)),
                ]
            ],
        ];
    }
    return $a;
 }
