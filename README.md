# Vitya CMS template

This package contains a basic website template built using Vitya CMS.

## How to setup a new application?

You can use Composer to create your app:

```
composer create-project vitya/cms-template my-new-project -s alpha
```

(NB: The -s alpha option is necessary for now, since there is no stable release
yet.)

Then you can launch your new app using PHP built-in web server:

```
php my-new-project/cli/server.php
```

By default, your application will be available at [http://localhost:8000](http://localhost:8000),
but you can specify another hostname or port number. For example, this will
make your website available to other machines on your network, on port 1234:

```
php my-new-project/cli/server.php 0.0.0.0:1234
```

WARNING: the built-in web server is only intended to be used as a development
tool! Do NOT use it in a production environment!

## How to access the admin interface?

The admin interface will be available at [http://localhost:8000/admin](http://localhost:8000/admin).
Your username is *admin* and your password is *123456789*.
