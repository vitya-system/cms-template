<?php

declare(strict_types=1);

use Vitya\Application\Application;
use Vitya\CmsUi\CmsUiExtension;
use Vitya\CmsComponent\CmsComponentsExtension;



// First, we need Composer autoloader, and we use it to bind the App namespace
// to the classes that are specific to this project.
$autoloader = require __DIR__ . '/../../vendor/autoload.php';
$autoloader->addPsr4('App\\', __DIR__ . '/src');



// Create an Application object and add the extensions.
$app = new Application(__DIR__);
$app->addExtension(new CmsComponentsExtension($app));
$app->addExtension(new CmsUiExtension($app));



// Load the config files.
// * config.json is the main application config file. It should only contain
//   non-sensitive values that don't depend on the environment --- ie. they
//   are the same on your development server and on the production server.
// * config.local.json is your local config file. It can contain values that are
//   specific to the environment. It can also override default config values. It
//   should not be versioned, since it depends on the environment.
$config_files = [
    __DIR__ . '/config/config.json',
    __DIR__ . '/config/config.local.json'
];
foreach ($config_files as $config_file) {
    if (file_exists($config_file)) {
        $app->loadConfigFromJsonFile($config_file);
    }
}



// If you need to define sensitive configuration variables, such as API keys
// or database credentials, you can also define them as environment variables,
// for example in your web server configuration file. Then you can inject
// them in the configuration array, like this:
// if (getenv('API_SECRET_KEY') !== false) {
//     $app->mergeConfig(['api_secret_key' => getenv('API_SECRET_KEY')]);
// }



// Now that the application knows all its configuration directives, we ask it to
// setup its parameters and services. This method will also make sure that every
// extension do the same. Note that the services won't be instantiated right
// now, but only when they are used for the first time.
$app->parametrize();



// Here is the good spot to define your own application routes, parameters and
// services:
// $app->loadRoutesFromJsonFile(__DIR__ . '/routes.json');
// $app['my_parameter'] = '123abc';
// $app->register(new MyServiceProvider());
$app->loadRoutesFromJsonFile(realpath(__DIR__ . '/routes.json'));



// After this point, the application will be immutable. We want to make sure
// that its configuration cannot be altered while it's running.
$app->makeImmutable();



// Initializes the application. The main goal of this method is for the
// application and its extensions to add middleware, event listeners, and
// commands.
$app->init();



// Here is the good spot to add your own middleware and event listeners:
// $app->get('request_handler')->addMiddleware((new MyMiddleware(), 100);
// $app->get('event_dispatcher')->addListenerProvider(new MyListenerProvider(), 100);
// It's also the last place where you can put additional initialization stuff
// before the application really starts to run.



// Your app is now ready to run!
return $app;
