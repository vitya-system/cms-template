<?php

declare(strict_types=1);

namespace App\MyApp;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Vitya\CmsComponent\Controller\AbstractController;

class BaseHtmlPage extends AbstractController
{
    private $locale = '';
    private $pageTitle = '';
    private $alternates = [];

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): BaseHtmlPage
    {
        $this->locale = $locale;
        return $this;
    }

    public function getPageTitle(): string
    {
        return $this->pageTitle;
    }

    public function setPageTitle(string $page_title): BaseHtmlPage
    {
        $this->pageTitle = $page_title;
        return $this;
    }

    public function getAlternates(): array
    {
        return $this->alternates;
    }

    public function setAlternate(string $hreflang, UriInterface $uri): BaseHtmlPage
    {
        $this->alternates[$hreflang] = $uri;
        return $this;
    }

    protected function render(string $template, array $parameters = [], ResponseInterface $response = null): ResponseInterface
    {
        $parameters = [
            'locale' => $this->getLocale(),
            'page_title' => $this->getPageTitle(),
            'alternates' => $this->getAlternates(),
        ] + $parameters;
        return parent::render($template, $parameters, $response);
    }

    public function assertValidLocale(string $locale): void
    {
        $locale_catalog = $this->getServiceFromHint('Vitya\Component\L10n\LocaleCatalog');
        if (false === $locale_catalog->hasLocale($locale)) {
            throw new Exception('Invalid locale: ' . $locale . '.');
        }
    }

}
