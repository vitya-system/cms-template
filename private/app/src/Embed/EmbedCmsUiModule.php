<?php

declare(strict_types=1);

namespace App\Embed;

use Vitya\CmsUi\EntityCmsUiModule\AbstractEmbedEntityCmsUiModule;

class EmbedCmsUiModule extends AbstractEmbedEntityCmsUiModule
{
    public static function getSupportedClass(): string
    {
        return 'App\Embed\Embed';
    }

    public static function getEntityTypeHumanName(): string
    {
        return 'embed';
    }

    public static function getEntityCollectionHumanName(): string
    {
        return 'embeds';
    }

    public function getEditionFormMainZoneDescription(): array
    {
        $description = [];
        return $description;
    }

}
