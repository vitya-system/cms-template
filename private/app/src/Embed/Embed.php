<?php

declare(strict_types=1);

namespace App\Embed;

use Vitya\CmsComponent\Entity\AbstractEmbedEntity;

class Embed extends AbstractEmbedEntity
{
    public static function getMachineName(): string
    {
        return 'embed';
    }

    public static function getCollectionMachineName(): string
    {
        return 'embeds';
    }

    public function getDataDescription(): array
    {
        $description = [];
        return $description;
    }

    public function getLocalizedTitle(string $locale): string
    {
        $title = '';
        if ('' === $title) {
            $title = 'Embed #' . $this->getId();
        } 
        return $title;
    }

}
