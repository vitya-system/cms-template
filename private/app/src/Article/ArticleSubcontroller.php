<?php

declare(strict_types=1);

namespace App\Article;

use Psr\Http\Message\ResponseInterface;
use Vitya\CmsComponent\Controller\AbstractController;

class ArticleSubcontroller extends AbstractController
{
    public function card(string $locale, int $id): ResponseInterface
    {
        $article = $this->ef()->load('App\Article\Article', $id);
        return $this->render('Article/card.twig', [
            'locale' => $locale,
            'article' => $article,
        ]);
    }

}
