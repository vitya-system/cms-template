<?php

declare(strict_types=1);

namespace App\Article;

use Vitya\CmsUi\EntityCmsUiModule\AbstractEntryEntityCmsUiModule;

class ArticleCmsUiModule extends AbstractEntryEntityCmsUiModule
{
    public static function getSupportedClass(): string
    {
        return 'App\Article\Article';
    }

    public static function getEntityTypeHumanName(): string
    {
        return 'article';
    }

    public static function getEntityCollectionHumanName(): string
    {
        return 'articles';
    }

    public function getEditionFormMainZoneDescription(): array
    {
        $description = [
            'title' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\PlainTextEditionFormWidget',
                'data' => ['title'],
                'options' => [
                    'title' => 'Title',
                ],
            ],
            'author' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\PlainTextEditionFormWidget',
                'data' => ['author'],
                'options' => [
                    'title' => 'Author',
                ],
            ],
            'teaser' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\CkeEditionFormWidget',
                'data' => ['teaser'],
                'options' => [
                    'title' => 'Teaser',
                ],
            ],
            'body' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\CompositeListEditionFormWidget',
                'data' => ['body'],
                'options' => [
                    'title' => 'Body',
                ],
            ],
            'main_image' => [
                'class' => 'Vitya\CmsUi\DatumEditionFormWidget\LinkedEntitiesEditionFormWidget',
                'data' => ['main_image'],
                'options' => [
                    'title' => 'Main image',
                ],
            ],
        ];
        return $description;
    }

    public function getSortOptionsDescription(): array
    {
        $sort_options_description = parent::getSortOptionsDescription();
        $sort_options_description['title'] = [
            'class' => 'Vitya\CmsUi\DisplayBoxSortOption\PlainTextDisplayBoxSortOption',
            'options' => [
                'label' => 'title',
                'datum' => 'title/' . $this->getCmsUi()->getEnv('locale'),
            ],
        ];
        return $sort_options_description;
    }

}
