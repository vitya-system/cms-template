<?php

declare(strict_types=1);

namespace App\Article;

use App\MyApp\BaseHtmlPage;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class ArticleController extends BaseHtmlPage
{
    public function view(string $locale, int $id): ResponseInterface
    {
        try {
            $this->assertValidLocale($locale);
            $article = $this->ef()->load('App\Article\Article', $id);
        } catch (Throwable $t) {
            $this->notFound($t->getMessage());
        }
        $this->setLocale($locale);
        $this->setPageTitle($article->getLocalizedTitle($locale));
        foreach ($this->getAppParam('l10n__locales') as $locale_k => $locale_v) {
            $this->setAlternate($locale_k, $this->uri('article-view', ['locale' => $locale_k, 'id' => $article->getId()]));
        }
        return $this->render('Article/view.twig', [
            'article' => $article,
        ]);
    }

}
