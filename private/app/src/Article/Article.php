<?php

declare(strict_types=1);

namespace App\Article;

use Vitya\CmsComponent\Entity\AbstractEntryEntity;

class Article extends AbstractEntryEntity
{
    public static function getMachineName(): string
    {
        return 'article';
    }

    public static function getCollectionMachineName(): string
    {
        return 'articles';
    }

    public function getOptions(): array
    {
        return [
            'use_publication' => true,
        ];
    }

    public function getDataDescription(): array
    {
        $description = [
            'title' => [
                'class' => '\Vitya\CmsComponent\Datum\PlainTextDatum',
                'localized' => true,
                'options' => [
                    'mandatory' => true,
                    'indexed' => true,
                ],
            ],
            'author' => [
                'class' => '\Vitya\CmsComponent\Datum\PlainTextDatum',
                'localized' => false,
                'options' => [],
            ],
            'teaser' => [
                'class' => '\Vitya\CmsComponent\Datum\HtmlTextDatum',
                'localized' => true,
                'options' => [],
            ],
            'body' => [
                'class' => '\Vitya\CmsComponent\Datum\CompositeListDatum',
                'localized' => false,
                'options' => [
                    'supported_classes' => [
                        'App\Composite\TextComposite\TextComposite',
                    ],
                ],
            ],
            'main_image' => [
                'class' => 'Vitya\CmsComponent\Datum\LinkedEntitiesDatum',
                'localized' => false,
                'options' => [
                    'supported_classes' => [
                        'App\Image\Image',
                    ],
                ],
            ],
        ];
        return $description;
    }

    public function getLocalizedTitle(string $locale): string
    {
        $title = trim($this->getComponent('title')->getChild($locale)->get());
        if ('' === $title) {
            $title = 'Article #' . $this->getId();
        } 
        return $title;
    }

}
