<?php

declare(strict_types=1);

namespace App\File;

use Vitya\CmsUi\EntityCmsUiModule\AbstractFileEntityCmsUiModule;

class FileCmsUiModule extends AbstractFileEntityCmsUiModule
{
    public static function getSupportedClass(): string
    {
        return 'App\File\File';
    }

    public static function getEntityTypeHumanName(): string
    {
        return 'file';
    }

    public static function getEntityCollectionHumanName(): string
    {
        return 'files';
    }

    public function getEditionFormMainZoneDescription(): array
    {
        $description = [
            'title' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\PlainTextEditionFormWidget',
                'data' => ['title'],
                'options' => [
                    'title' => 'Title',
                ],
            ],
        ];
        return $description;
    }

}
