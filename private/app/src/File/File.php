<?php

declare(strict_types=1);

namespace App\File;

use Vitya\CmsComponent\Entity\AbstractFileEntity;

class File extends AbstractFileEntity
{
    public static function getMachineName(): string
    {
        return 'file';
    }

    public static function getCollectionMachineName(): string
    {
        return 'files';
    }

    public function getDataDescription(): array
    {
        $description = [
            'title' => [
                'class' => '\Vitya\CmsComponent\Datum\PlainTextDatum',
                'localized' => true,
                'options' => [],
            ],
        ];
        return $description;
    }

    public function getLocalizedTitle(string $locale): string
    {
        $title = trim($this->getComponent('title')->getChild($locale)->get());
        if ('' === $title) {
            $title = 'File #' . $this->getId();
        } 
        return $title;
    }

}
