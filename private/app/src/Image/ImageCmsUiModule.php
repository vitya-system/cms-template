<?php

declare(strict_types=1);

namespace App\Image;

use Vitya\CmsUi\EntityCmsUiModule\AbstractImageEntityCmsUiModule;

class ImageCmsUiModule extends AbstractImageEntityCmsUiModule
{
    public static function getSupportedClass(): string
    {
        return 'App\Image\Image';
    }

    public static function getEntityTypeHumanName(): string
    {
        return 'image';
    }

    public static function getEntityCollectionHumanName(): string
    {
        return 'images';
    }

    public function getEditionFormMainZoneDescription(): array
    {
        $description = [
            'legend' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\PlainTextEditionFormWidget',
                'data' => ['legend'],
                'options' => [
                    'title' => 'Legend',
                ],
            ],
        ];
        return $description;
    }

}
