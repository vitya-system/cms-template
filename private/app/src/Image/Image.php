<?php

declare(strict_types=1);

namespace App\Image;

use Vitya\CmsComponent\Entity\AbstractImageEntity;

class Image extends AbstractImageEntity
{
    public static function getMachineName(): string
    {
        return 'image';
    }

    public static function getCollectionMachineName(): string
    {
        return 'images';
    }

    public function getDataDescription(): array
    {
        $description = [
            'legend' => [
                'class' => '\Vitya\CmsComponent\Datum\PlainTextDatum',
                'localized' => true,
                'options' => [],
            ],
        ];
        return $description;
    }

    public function getLocalizedTitle(string $locale): string
    {
        $title = trim($this->getComponent('legend')->getChild($locale)->get());
        if ('' === $title) {
            $title = 'Image #' . $this->getId();
        } 
        return $title;
    }

}
