<?php

declare(strict_types=1);

namespace App\Homepage;

use App\MyApp\BaseHtmlPage;
use Psr\Http\Message\ResponseInterface;
use Throwable;
use Vitya\CmsComponent\EntityListModifier\EntitySort;
use Vitya\CmsComponent\EntityListModifier\PublicationFilter;

class HomepageController extends BaseHtmlPage
{
    public function frontpage()
    {
        $this->seeOther($this->uri('homepage', ['locale' => 'en']));
    }

    public function index(string $locale): ResponseInterface
    {
        try {
            $this->assertValidLocale($locale);
            $welcome_static_page = $this->ef()->load('App\StaticPage\StaticPage', 1);
        } catch (Throwable $t) {
            $this->notFound($t->getMessage());
        }
        $this->setLocale($locale);
        foreach ($this->getAppParam('l10n__locales') as $locale_k => $locale_v) {
            $this->setAlternate($locale_k, $this->uri('homepage', ['locale' => $locale_k]));
        }
        $article_model = $this->ef()->make('App\Article\Article');
        $article_ids = $article_model
            ->getEntityList()
            ->addModifier(new PublicationFilter('publication/en', 'currently_published'))
            ->addModifier(new EntitySort('id', 'DESC'))
            ->setMaxResults(5)
            ->getIds()
        ;
        return $this->render('Homepage/homepage.twig', [
            'welcome_static_page' => $welcome_static_page,
            'article_ids' => $article_ids,
        ]);
    }

}
