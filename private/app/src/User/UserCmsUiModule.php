<?php

declare(strict_types=1);

namespace App\User;

use Vitya\CmsUi\EntityCmsUiModule\AbstractUserEntityCmsUiModule;

class UserCmsUiModule extends AbstractUserEntityCmsUiModule
{
    public static function getSupportedClass(): string
    {
        return 'App\User\User';
    }

    public static function getEntityTypeHumanName(): string
    {
        return 'user';
    }

    public static function getEntityCollectionHumanName(): string
    {
        return 'users';
    }

    public function getEditionFormMainZoneDescription(): array
    {
        $description = [
            'first_name' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\PlainTextEditionFormWidget',
                'data' => ['first_name'],
                'options' => [
                    'title' => 'First name',
                ],
            ],
            'last_name' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\PlainTextEditionFormWidget',
                'data' => ['last_name'],
                'options' => [
                    'title' => 'Last name',
                ],
            ],
        ];
        return $description;
    }

}
