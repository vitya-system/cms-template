<?php

declare(strict_types=1);

namespace App\User;

use Vitya\CmsComponent\Entity\AbstractUserEntity;

class User extends AbstractUserEntity
{
    public static function getMachineName(): string
    {
        return 'user';
    }

    public static function getCollectionMachineName(): string
    {
        return 'users';
    }

    public function getDataDescription(): array
    {
        $description = [
            'first_name' => [
                'class' => '\Vitya\CmsComponent\Datum\PlainTextDatum',
                'localized' => false,
                'options' => [
                    'mandatory' => true,
                ],
            ],
            'last_name' => [
                'class' => '\Vitya\CmsComponent\Datum\PlainTextDatum',
                'localized' => false,
                'options' => [],
            ],
        ];
        return $description;
    }

    public function getLocalizedTitle(string $locale): string
    {
        $title = trim($this->getComponent('first_name')->get());
        if ('' !== $title) {
            $title .= ' ';
        }
        $title .= trim($this->getComponent('last_name')->get());
        $title = trim($title);
        if ('' === $title) {
            $title = 'User #' . $this->getId();
        }
        return $title;
    }

}
