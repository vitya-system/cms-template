<?php

declare(strict_types=1);

namespace App\Configuration;

use Vitya\CmsUi\EntityCmsUiModule\AbstractSingletonEntityCmsUiModule;

class ConfigurationCmsUiModule extends AbstractSingletonEntityCmsUiModule
{
    public static function getSupportedClass(): string
    {
        return 'App\Configuration\Configuration';
    }

    public static function getEntityTypeHumanName(): string
    {
        return 'configuration';
    }

    public static function getEntityCollectionHumanName(): string
    {
        return 'configuration';
    }

    public function getEditionFormMainZoneDescription(): array
    {
        $description = [
            'website_title' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\PlainTextEditionFormWidget',
                'data' => ['website_title'],
                'options' => [
                    'title' => 'Website title',
                ],
            ],
            'website_description' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\PlainTextEditionFormWidget',
                'data' => ['website_description'],
                'options' => [
                    'title' => 'Website description',
                ],
            ],
            'color' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\PlainTextEditionFormWidget',
                'data' => ['color'],
                'options' => [
                    'title' => 'Color',
                ],
            ],
            'theme' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\PlainTextEditionFormWidget',
                'data' => ['theme'],
                'options' => [
                    'title' => 'Theme',
                ],
            ],
        ];
        return $description;
    }

}
