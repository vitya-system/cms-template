<?php

declare(strict_types=1);

namespace App\Configuration;

use Vitya\CmsComponent\Entity\AbstractSingletonEntity;

class Configuration extends AbstractSingletonEntity
{
    public static function getCollectionMachineName(): string
    {
        return 'configuration';
    }

    public function getDataDescription(): array
    {
        $description = [
            'website_title' => [
                'class' => '\Vitya\CmsComponent\Datum\PlainTextDatum',
                'localized' => true,
                'options' => [],
            ],
            'website_description' => [
                'class' => '\Vitya\CmsComponent\Datum\PlainTextDatum',
                'localized' => true,
                'options' => [],
            ],
            'color' => [
                'class' => '\Vitya\CmsComponent\Datum\PlainTextDatum',
                'localized' => false,
                'options' => [],
            ],
            'theme' => [
                'class' => '\Vitya\CmsComponent\Datum\PlainTextDatum',
                'localized' => false,
                'options' => [],
            ],
        ];
        return $description;
    }

}
