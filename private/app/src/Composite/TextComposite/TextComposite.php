<?php

declare(strict_types=1);

namespace App\Composite\TextComposite;

use Vitya\CmsComponent\Composite\AbstractComposite;

class TextComposite extends AbstractComposite
{
    public function getDataDescription(): array
    {
        $description = [
            'html' => [
                'class' => '\Vitya\CmsComponent\Datum\HtmlTextDatum',
                'localized' => true,
                'options' => [],
            ],
        ];
        return $description;
    }

}
