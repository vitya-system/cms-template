<?php

declare(strict_types=1);

namespace App\Composite\TextComposite;

use Vitya\CmsUi\CompositeCmsUiModule\AbstractCompositeCmsUiModule;

class TextCompositeCmsUiModule extends AbstractCompositeCmsUiModule
{
    public static function getSupportedClass(): string
    {
        return 'App\Composite\TextComposite\TextComposite';
    }

    public static function getCOmpositeTypeHumanName(): string
    {
        return 'Text block';
    }

    public function getEditionFormBlockDescription(): array
    {
        $description = [
            'html' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\CkeEditionFormWidget',
                'data' => ['html'],
                'options' => [
                    'title' => 'Text',
                ],
            ],
        ];
        return $description;
    }

}
