<?php

declare(strict_types=1);

namespace App\NavigationMenu;

use Vitya\CmsUi\EntityCmsUiModule\AbstractNodeEntityCmsUiModule;

class NavigationMenuItemCmsUiModule extends AbstractNodeEntityCmsUiModule
{
    public static function getSupportedClass(): string
    {
        return 'App\NavigationMenu\NavigationMenuItem';
    }

    public static function getEntityTypeHumanName(): string
    {
        return 'navigation menu item';
    }

    public static function getEntityCollectionHumanName(): string
    {
        return 'navigation menu';
    }

    public function getEditionFormMainZoneDescription(): array
    {
        $description = [
            'title' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\PlainTextEditionFormWidget',
                'data' => ['title'],
                'options' => [
                    'title' => 'Title',
                ],
            ],
            'url' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\PlainTextEditionFormWidget',
                'data' => ['url'],
                'options' => [
                    'title' => 'Url',
                ],
            ],
        ];
        return $description;
    }

}
