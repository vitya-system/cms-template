<?php

declare(strict_types=1);

namespace App\NavigationMenu;

use Psr\Http\Message\ResponseInterface;
use Vitya\CmsComponent\Controller\AbstractController;
use Vitya\CmsComponent\Tree\TreeStructure;

class NavigationMenuSubcontroller extends AbstractController
{
    public function menu(string $locale): ResponseInterface
    {
        $navigation_menu_item_model = $this->ef()->make('App\NavigationMenu\NavigationMenuItem');
        $navigation_menu_item_list = $navigation_menu_item_model->getEntityList();
        $navigation_menu_item_ids = $navigation_menu_item_list->getIds();
        $navigation_menu_items = $navigation_menu_item_model->loadMultiple($navigation_menu_item_ids);
        $collection_info = $navigation_menu_item_model->getCollectionInfo();
        $associative_map = $collection_info->get('tree_structure');
        if (false === is_array($associative_map)) {
            $associative_map = [];
        }
        $tree_structure = new TreeStructure();
        $tree_structure->loadFromAssociativeMap($associative_map, $navigation_menu_item_ids);
        $hierarchy = $tree_structure->getHierarchy();
        return $this->render('NavigationMenu/menu.twig', [
            'locale' => $locale,
            'navigation_menu_items' => $navigation_menu_items,
            'hierarchy' => $hierarchy,
        ]);
    }

}
