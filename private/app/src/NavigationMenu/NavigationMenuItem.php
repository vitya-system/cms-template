<?php

declare(strict_types=1);

namespace App\NavigationMenu;

use Vitya\CmsComponent\Entity\AbstractNodeEntity;

class NavigationMenuItem extends AbstractNodeEntity
{
    public static function getMachineName(): string
    {
        return 'navigation-menu-item';
    }

    public static function getCollectionMachineName(): string
    {
        return 'navigation-menu';
    }

    public function getDataDescription(): array
    {
        $description = [
            'title' => [
                'class' => '\Vitya\CmsComponent\Datum\PlainTextDatum',
                'localized' => true,
                'options' => [
                    'mandatory' => true,
                ],
            ],
            'url' => [
                'class' => '\Vitya\CmsComponent\Datum\PlainTextDatum',
                'localized' => true,
                'options' => [],
            ],
        ];
        return $description;
    }

    public function getLocalizedTitle(string $locale): string
    {
        $title = trim($this->getComponent('title')->getChild($locale)->get());
        if ('' === $title) {
            $title = 'Navigation menu item #' . $this->getId();
        } 
        return $title;
    }

}
