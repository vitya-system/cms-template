<?php

declare(strict_types=1);

namespace App\StaticPage;

use Vitya\CmsUi\EntityCmsUiModule\AbstractEntryEntityCmsUiModule;

class StaticPageCmsUiModule extends AbstractEntryEntityCmsUiModule
{
    public static function getSupportedClass(): string
    {
        return 'App\StaticPage\StaticPage';
    }

    public static function getEntityTypeHumanName(): string
    {
        return 'static page';
    }

    public static function getEntityCollectionHumanName(): string
    {
        return 'static pages';
    }

    public function getEditionFormMainZoneDescription(): array
    {
        $description = [
            'title' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\PlainTextEditionFormWidget',
                'data' => ['title'],
                'options' => [
                    'title' => 'Title',
                ],
            ],
            'teaser' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\CkeEditionFormWidget',
                'data' => ['teaser'],
                'options' => [
                    'title' => 'Teaser',
                ],
            ],
            'body' => [
                'class' => '\Vitya\CmsUi\DatumEditionFormWidget\CkeEditionFormWidget',
                'data' => ['body'],
                'options' => [
                    'title' => 'Body',
                ],
            ],
        ];
        return $description;
    }

    public function getSortOptionsDescription(): array
    {
        $sort_options_description = parent::getSortOptionsDescription();
        $sort_options_description['title'] = [
            'class' => 'Vitya\CmsUi\DisplayBoxSortOption\PlainTextDisplayBoxSortOption',
            'options' => [
                'label' => 'title',
                'datum' => 'title/' . $this->getCmsUi()->getEnv('locale'),
            ],
        ];
        return $sort_options_description;
    }

}
