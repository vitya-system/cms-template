<?php

declare(strict_types=1);

namespace App\StaticPage;

use Vitya\CmsComponent\Entity\AbstractEntryEntity;

class StaticPage extends AbstractEntryEntity
{
    public static function getMachineName(): string
    {
        return 'static-page';
    }

    public static function getCollectionMachineName(): string
    {
        return 'static-pages';
    }

    public function getOptions(): array
    {
        return [
            'use_publication' => true,
        ];
    }

    public function getDataDescription(): array
    {
        $description = [
            'title' => [
                'class' => '\Vitya\CmsComponent\Datum\PlainTextDatum',
                'localized' => true,
                'options' => [
                    'mandatory' => true,
                ],
            ],
            'teaser' => [
                'class' => '\Vitya\CmsComponent\Datum\HtmlTextDatum',
                'localized' => true,
                'options' => [],
            ],
            'body' => [
                'class' => '\Vitya\CmsComponent\Datum\HtmlTextDatum',
                'localized' => true,
                'options' => [],
            ],
        ];
        return $description;
    }

    public function getLocalizedTitle(string $locale): string
    {
        $title = trim($this->getComponent('title')->getChild($locale)->get());
        if ('' === $title) {
            $title = 'Static page #' . $this->getId();
        } 
        return $title;
    }

}
