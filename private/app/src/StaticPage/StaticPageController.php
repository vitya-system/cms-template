<?php

declare(strict_types=1);

namespace App\StaticPage;

use App\MyApp\BaseHtmlPage;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class StaticPageController extends BaseHtmlPage
{
    public function view(string $locale, int $id): ResponseInterface
    {
        try {
            $this->assertValidLocale($locale);
            $static_page = $this->ef()->load('App\StaticPage\StaticPage', $id);
        } catch (Throwable $t) {
            $this->notFound($t->getMessage());
        }
        $this->setLocale($locale);
        $this->setPageTitle($static_page->getLocalizedTitle($locale));
        foreach ($this->getAppParam('l10n__locales') as $locale_k => $locale_v) {
            $this->setAlternate($locale_k, $this->uri('static-page-view', ['locale' => $locale_k, 'id' => $static_page->getId()]));
        }
        return $this->render('StaticPage/view.twig', [
            'static_page' => $static_page,
        ]);
    }

}
