# Changelog

## [1.0.0-alpha13] - 2025-02-27
### Changed
- Update Vitya system to alpha13.
- Refine the application.

## [1.0.0-alpha12] - 2024-09-30
### Changed
- Update Vitya system to alpha12.
- Refine the application.

## [1.0.0-alpha11] - 2024-07-16
### Changed
- Update Vitya system to alpha11.

## [1.0.0-alpha10] - 2022-07-01
### Changed
- Update Vitya system to alpha10.
- Make use of image entities.

## [1.0.0-alpha9] - 2022-06-03
### Changed
- Update Vitya system to alpha9.

## [1.0.0-alpha8] - 2021-08-02
### Changed
- Update Vitya system to alpha8.

## [1.0.0-alpha7] - 2021-07-05
### Changed
- Template adapted to alpha7 libs.

## [1.0.0-alpha6] - 2021-06-07
### Changed
- Template adapted to alpha6 libs.

## [1.0.0-alpha5] - 2021-05-03
### Changed
- Template adapted to alpha5 libs.

## [1.0.0-alpha4] - 2021-04-07
### Changed
- Bumped version to alpha4 to match the rest of the system.

## [1.0.0-alpha3] - 2021-03-01
### Changed
- Serve gzipped assets.
- Update PHP dependencies.

## [1.0.0-alpha2] - 2021-02-01
### Changed
- The Article abstract class was renamed as ArticleSdbcu.
- Exclude node_modules from versioning.

## [1.0.0-alpha1] - 2021-01-04
### Added
- First release.
